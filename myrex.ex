defmodule Myrex do

  def format(tokens) do
    Enum.map(tokens, fn {token, _, str} ->
    case token do
      :int -> "<span class=\"int\">#{str}</span>"
      :comillas -> "<span class=\"comillas\">#{str}</span>"
      :dotss -> "<span class=\"dotss\">#{str}</span>"
      :punto -> "<span class=\"punto\">#{str}</span>"
      :tab -> "<blockquote>"
      :comentario -> "<span class=\"comment\">#{str}</span>"
      :terminator -> "<span class=\"terminator\"><br></span>"
      :float -> "<span class=\"float\">#{str}</span>"
      :bool -> "<span class=\"bool\">#{str}</span>"
      :bothparen-> "<span class=\"parentesis\">#{str}</span>"
      :llave -> "<span class=\"llaves\">#{str}</span>"
      :corchetes -> "<span class=\"corchetes\">#{str}</span>"
      :punctuation -> "<span class=\"punctuation\">#{str}</span>"
      :keyword -> "<span class=\"keyword\">#{str}</span>"
      :macros -> "<span class=\"macros\">#{str}</span>"
      :isiden -> "<span class=\"isiden\">#{str}</span>"
      :modules-> "<span class=\"modules\">#{str}</span>"
      :signed_int_type -> "<span class=\"signed_int_type\">#{str}</span>"
      :unsigned_int_type -> "<span class=\"unsigned_int_type\">#{str}</span>"
      :primitivo_type -> "<span class=\"primitivo_type\">#{str}</span>"
      :struc -> "<span class=\"struct\">#{str}</span>"
      :isfunction -> "<span class=\"isfunction\">#{str}</span>"
      :ismodulefromresult -> "<span class=\"ismodulefromresult\">#{str}</span>"
      :identifier -> "<span class=\"identifier\">#{str}</span>"
    end
    end)
end
  def main(args\\[]) do
    html=hd(args)
    |> File.read!()
    |> to_charlist()
    |> :lexer.string()
    |> elem(1)
    |> format()
    |> Enum.join("\n")
    IO.puts("""
            <html>
              <head>
                <link rel="stylesheet" href="colores.css" type="text/css"/>
              </head>
              <body>
                #{html}
              </body>
            </html>
            """)
  end
end
