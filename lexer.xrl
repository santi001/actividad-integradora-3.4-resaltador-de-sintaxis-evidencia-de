Definitions.
D = [0-9]
L = [a-z]
Whitespace = [\s]
Whitespace2 = \s\t|\t
Terminator = \n|\s
Dots = ,|;|:
Lparen = [\(]
Rparent = [\)]
Comment =  \/\/\!|\/\/\!\!|\/\*\!|\/\*\!\!|\/\/|\/\/\/|\/\/\/\/|\/\*|\/\*\*|\/\*\*\*|\*\/
Llaves = \{|\}
Corchetes = \[|\]
Punctuation = \+|\-|\*|\/|\<|\>|\'%'|\^|\!|\&|\||\&\&|\|\||\<\<|\>\>|\+\=|\-\=|\*\=|\/\=|\'%'\=|\^\=|\&\=|\=\=|\&|\_|\>|\<|"'"|\=\>|\=|\#|\?|\¿|\$
Comilla = \"|\'
Bparent = \\
Pal = [a-zA-Z][a-zA-Z0-9]*

Rules.
{Comilla} : {token, {comillas, TokenLine, TokenChars}}.
{Dots} : {token, {dotss, TokenLine, TokenChars}}.
\t. : {token, {tab, TokenLine, TokenChars}}.
{Whitespace} : skip_token.
{Whitespace2} : {token, {tab, TokenLine, TokenChars}}.
\. : {token, {punto, TokenLine, TokenChars}}.
{Terminator} : {token, {terminator, TokenLine, TokenChars}}.
{Lparen}|{Rparent}|{Lparen}{Rparent} : {token, {bothparen, TokenLine, TokenChars}}.
(true)|(false) :{token,{bool,TokenLine, TokenChars}}.
\s : skip_token.
{Bparent}+ : {token,{bothparen,TokenLine, TokenChars}}. 
\/\/.+ : {token,{comentario,TokenLine, TokenChars}}.
{D}+ : {token,{int,TokenLine, TokenChars}}.
{D}+\.{D}+ : {token,{float, TokenLine, TokenChars}}.
{Comment}. : {token,{comentario, TokenLine, TokenChars}}.
{Llaves} : {token,{llave,TokenLine, TokenChars}}.
{Corchetes} : {token,{corchetes,TokenLine, TokenChars}}.
{Punctuation} : {token,{punctuation, TokenLine, TokenChars}}.
{Pal} : {token, analyze(TokenLine, TokenChars)}.

Erlang code.

analyze(TokenLine, TokenChars) ->
    Up = string:uppercase(TokenChars),
    IsKW = lists:member(Up, ["IF", "ELSE", "SELFTY", "AS", "ASYNC", "AWAIT", "BREAK",
        "CONST", "CONTINUE", "CRATE", "DYN", "ELSE", "ENUM", "EXTERN", "FALSE", "FN",
        "FOR", "IMPL", "IN", "LET", "LOOP", "MATCH", "MOD", "MOVE", "MUT", "PUB",
        "REF", "RETURN", "SELF", "STATIC", "STRUCT", "SUPER", "TRAIT", "TRUE", "TYPE",
        "UNSAFE", "USE", "WHERE", "WHILE", "ABSTRACT", "BECOME", "BOX", "DO", "FINAL",
        "MACRO", "OVERRIDE", "PRIV", "TYPEOF", "UNSIZED", "VIRTUAL", "YIELD", "MACRO_RULES",
        "UNION", "STATIC"]),
    IsId = lists:member(Up, ["FOO", "_IDENTIFIER", "R#TRUE", "MOCKBA", "東京"]),
    IsMc = lists:member(Up, ["ASSERT", "ASSERT_EQ", "ASSERT_NE", "CFG", "COLUMN", "COMPILE_ERROR",
        "CONCAT", "DBG", "DEBUG_ASSERT", "DEBUG_ASSERT_EQ", "DEBUG_ASSERT_NE", "ENV", "EPRINT", "EPRINTLN",
        "FILE", "FORMAT", "FORMAT_ARGS", "INCLUDE", "INCLUDE_BYTES", "INCLUDE_STR", "IS_X86_FEATURE_DETECTED",
        "LINE", "MATCHES", "MODULE_PATH", "OPTION_ENV", "PANIC", "PRINT", "PRINTLN", "STRINGIFY", "THREAD_LOCAL",
        "TODO", "TRY", "UNIMPLEMENTED", "UNREACHABLE","WRITE", "WRITELN"]),
    IsMd = lists:member(Up, ["ALLOC", "ANY", "ARCH", "ASCII", "BORROW", "BOXED", "CELL", "CHAR", "CLONE",
        "CMP", "COLLECTIONS", "CONVERT", "DEFAULT", "ENV", "ERROR", "HASH", "HINT", "ITER", "MARKER", "MEM", " NET",
        "NUM", "OPS", "OPTIN", "OS", "PANIC", "PATH", "PIN", "PRELUDE", "PRIMITIVE", "PROCESS", "PTR", "RC",
        "RESULT", "STR", "STRING", "SYNC", "TASK", "THREAD", "TIME", "VEC"]),
    SgInt = lists:member(Up, ["I8", "I16", "I32", "I64", "I128", "ISIZE"]),
    UsnInt = lists:member(Up, ["U8", "U16", "U32", "U64", "U128"]),
    PrimType = lists:member(Up, ["ARRAY", "BOOL", "CHAR", "POINTER", "REFERENCE", "SLICE", "STR", "TUPLE", "UNIT", "USIZE"]),
    IsStr = lists:member(Up, ["SCOPE", "SCOPEJOINHANDLE", "ACCESSERROR", "JOINHANDLE", "LOCALKEY", "THREAD", "THREADID"]),
    IsFunc = lists:member(Up, ["CURRENT", "PANICKING", "PARK", "PARK_TIMEOUT", "SLEEP", "SPAWN", "YIELD_NOW", "CHECKED_DIVISION"]),
    IdMd_Result = lists:member(Up, ["AND", "AND_THEN", "AS_DEREF", "AS_DEREF_MUT", "AS_MUT", "AS_REF", "CLONED", 
        "CONTAINS", "CONTAINS_ERR", "COPIED", "ERR", "EXPECT", "EXPECT_ERR", "FLATTEN", "INSPECT", "INTO_ERR", 
        "INSPECT_ERR", "INTO_ERR", "INTO_OK", "INTO_OK_OR_ERR", "IS_ERR", "IS_ERR_WITH", "IS_OK", "IS_OK_WITH", 
        "ITER", "ITER_MUT", "MAP", "MAP_ERR", "MAP_OR", "MAP_OR_ELSE", "OK", "OR", "OR_ELSE", "TRANSPOSE", "UNWRAP", 
        "UNWRAP_ERR", "UNWRAP_OR", "UNWRAP_OR_DEFAULT", "UNWRAP_UNCHECKED", "UNWRAP_OR_ELSE"]),
    if
        IsKW -> {keyword, TokenLine, TokenChars};
        IsMc -> {macros, TokenLine, TokenChars};
        IsId -> {isiden, TokenLine, TokenChars};
        IsMd -> {modules, TokenLine, TokenChars};
        SgInt -> {signed_int_type, TokenLine, TokenChars};
        UsnInt -> {unsigned_int_type, TokenLine, TokenChars};
        PrimType -> {primitivo_type, TokenLine, TokenChars};
        IsStr -> {struc, TokenLine, TokenChars};
        IsFunc -> {isfunction, TokenLine, TokenChars};
        IdMd_Result -> {ismodulefromresult, TokenLine, TokenChars};
        true -> {identifier, TokenLine, TokenChars}
    end.
